import React, { ReactNode } from 'react';

interface TitlePagesProps {
    children: ReactNode;
    head: string|undefined;
    nama: string|undefined;
}

const TitlePages: React.FC<TitlePagesProps> = ({ children , nama, head}) => {
    return (
        <>
            <main id="main" className="main">
                <div className="pagetitle">
                    <h1>{head}</h1>
                    <nav>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href={"/"}>Home</a></li>
                            <li className="breadcrumb-item active">{nama}</li>
                        </ol>
                    </nav>
                </div>
                {children}
            </main>
        </>
    );
};

export default TitlePages;