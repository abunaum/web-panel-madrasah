import React, {useState} from 'react';
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import axios from "axios";
import {getCookie} from "cookies-next";
import styles from "@/styles/card_profile.module.css";
import {Button, Modal} from "react-bootstrap";

const PasswordModal = () => {
    const [showPassword, setShowPassword] = useState<boolean>(false);
    const [plama, setPlama] = useState<string>("");
    const [pbaru, setPbaru] = useState<string>("");
    const [upbaru, setUpbaru] = useState<string>("");
    const handleMPassword = () => setShowPassword(true);
    const handleCloseMpassword = () => setShowPassword(false);
    const handleSetPassword = async () => {
        setShowPassword(false);
        const data: { password: string, new_password: string, confirm_password: string } = {
            password: plama,
            new_password: pbaru,
            confirm_password: upbaru
        }
        try {
            showWaitLoading('Mencoba mengubah password.');
            const req = await axios.put(`${process.env.NEXT_PUBLIC_API_URL}/user/password`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + getCookie('token') ?? ''
                }
            });
            if (req.data.success) {
                await LoadingTimer('Password berhasil diubah', 'success', 1500);
            } else {
                await LoadingTimer('Password gagal diubah', 'error', 1500);
                console.log(req.data)
            }
        } catch (e: any) {
            let msg: string;
            if (typeof e.response?.data?.message !== 'undefined') {
                msg = e.response.data.message;
            } else {
                msg = e.message;
            }
            await LoadingTimer(`Password gagal diubah<br/>${msg}`, 'error', 3000);
            console.log(e)
        }
    }
    return (
        <React.Fragment>
            <button className={styles.button}
                    onClick={handleMPassword}>Password
            </button>
            <Modal show={showPassword} onHide={handleCloseMpassword} centered={true}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Password</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-group mb-3">
                        <label htmlFor="plama">Password Lama</label>
                        <input type="password" className="form-control" id="plama" aria-describedby="plama"
                               placeholder="Password Lama" onChange={(e) => {
                            setPlama(e.target.value);
                        }} defaultValue={plama}/>
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="pbaru">Password Baru</label>
                        <input type="password" className="form-control" id="pbaru" placeholder="Password Baru"
                               onChange={(e) => {
                                   setPbaru(e.target.value);
                               }} defaultValue={pbaru}/>
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="upbaru">Konfirmasi Password Baru</label>
                        <input type="password" className="form-control" id="upbaru"
                               placeholder="Konfirmasi Password Baru" onChange={(e) => {
                            setUpbaru(e.target.value);
                        }} defaultValue={upbaru}/>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseMpassword}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleSetPassword}>
                        Edit
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    );
};

export default PasswordModal;