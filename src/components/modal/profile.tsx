import React, {useState} from 'react';
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import axios from "axios";
import {getCookie} from "cookies-next";
import styles from "@/styles/card_profile.module.css";
import {Button, Modal} from "react-bootstrap";
import {CldImage} from "next-cloudinary";
import Image from "next/image";

type ProfileModalProps = {
    data: any
}

const ProfileModal: React.FC<ProfileModalProps> = ({data}) => {
    const [showProfile, setShowProfile] = useState<boolean>(false);
    const [uploadedImageUrl, setUploadedImageUrl] = useState<string | null>(null);
    const [nama, setNama] = useState<string>(data.nama);
    const [username, setUsername] = useState<string>(data.username);
    const [email, setEmail] = useState<string>(data.email);
    const [gambar, setGambar] = useState<string>(data.gambar ?? 'website/gs/gpbe2jttvnhh1egbw6in');
    const handleMProfile = () => setShowProfile(true);
    const handleCloseMprofile = () => {
        setShowProfile(false)
        setUploadedImageUrl(null)
    };
    const handleSetProfile = async () => {
        setShowProfile(false);
        let newGambar = gambar;
        if (uploadedImageUrl) {
            console.log("upload gambar")
            showWaitLoading('Mengupload gambar.')
            const formData = new FormData();
            const file = document.getElementById("foto") as HTMLInputElement;
            formData.append("file", file.files?.[0] as Blob);
            formData.append("location", "user");
            if (file.files?.[0]) {
                try {
                    const upload = await axios.post('/api/upload', formData, {
                        headers: {
                            "Content-Type": "multipart/form-data",
                        }
                    });
                    if (upload.data.success) {
                        newGambar = upload.data.data.public_id;
                        await LoadingTimer('Berhasil mengupload gambar.', 'success', 1500);
                    } else {
                        await LoadingTimer('Gagal mengupload gambar.', 'error', 1500);
                        console.log(upload.data.message);
                        return;
                    }
                } catch (e) {
                    await LoadingTimer('Gagal mengupload gambar.', 'error', 1500);
                    console.log(e);
                    return;
                }
            }
        }
        if (nama === "" && username === "" && email === "") {
            await LoadingTimer('Data tidak lengkap.', 'error', 1500);
            return;
        }
        const data = {
            nama: nama,
            username: username,
            email: email,
            gambar: newGambar
        }
        try {
            showWaitLoading('Mencoba mengubah profile.');
            const req = await axios.put(`${process.env.NEXT_PUBLIC_API_URL}/user/profile`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + getCookie('token') ?? ''
                }
            });
            if (req.data.success) {
                await LoadingTimer('Password berhasil diubah', 'success', 1500);
                window.location.reload();
            } else {
                await LoadingTimer('Password gagal diubah', 'error', 1500);
                console.log(req.data)
            }
        } catch (e: any) {
            let msg: string;
            if (typeof e.response?.data?.message !== 'undefined') {
                msg = e.response.data.message;
            } else {
                msg = e.message;
            }
            await LoadingTimer(`Password gagal diubah<br/>${msg}`, 'error', 3000);
            console.log(e)
        }
    }

    const handleImageUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = (e) => {
                setUploadedImageUrl(e.target?.result as string);
            };

            reader.readAsDataURL(file);
        } else {
            setUploadedImageUrl(null);
        }
    }
    return (
        <React.Fragment>
            <button className={`${styles.button} ${styles.msg}`}
                    onClick={handleMProfile}>Profile
            </button>
            <Modal show={showProfile} onHide={handleCloseMprofile} centered={true}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Profile</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-group mb-3">
                        <label htmlFor="nama">Nama</label>
                        <input type="text" className="form-control" id="nama" aria-describedby="nama"
                               placeholder="Nama" onChange={(e) => {
                            setNama(e.target.value);
                        }} defaultValue={nama}/>
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" placeholder="Username"
                               onChange={(e) => {
                                   setUsername(e.target.value);
                               }} defaultValue={username}/>
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="email">Email</label>
                        <input type="email" className="form-control" id="email"
                               placeholder="Email" onChange={(e) => {
                            setEmail(e.target.value);
                        }} defaultValue={email}/>
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="foto" className="form-label">Gambar</label>
                        <input className="form-control mb-2" type="file" id="foto" name="foto" onChange={async (e)=>{
                            await handleImageUpload(e)
                        }} />
                        {uploadedImageUrl ? (
                            <Image
                                id={"image-preview"}
                                src={uploadedImageUrl}
                                alt="gambar"
                                className={"img-thumbnail"}
                                width="1200"
                                height="900"
                                priority
                                style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                            />
                        ): (
                            <CldImage
                                width="1200"
                                height="900"
                                className="img-thumbnail"
                                src={gambar}
                                alt="gambar"
                                priority
                                style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                            />
                        )}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseMprofile}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleSetProfile}>
                        Edit
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    );
};

export default ProfileModal;