import React, {useState} from 'react';
import {Modal, Button} from 'react-bootstrap';
import {decryptData} from '@/utils/decrypt';
import {uploadBackup, Reset} from '@/utils/uploadbackup';
import {LoadingTimer, showWaitLoading, showWaitPercent} from "@/components/waitLoading";

type RestoreModalProps = {
    type: "gs" | "post";
    reload: () => void;
}
const RestoreModal: React.FC<RestoreModalProps> = ({type, reload}) => {
    const [show, setShow] = useState(false);
    const [percetage, setPercentage] = useState<number | boolean>(false);
    const [selectedFile, setSelectedFile] = useState<File | null>(null);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.files && event.target.files.length > 0) {
            setSelectedFile(event.target.files[0]);
        }
    };

    React.useEffect(() => {
        if (percetage){
            const progressElement = document.getElementById('persentasenya') as HTMLElement;
            const progress = document.querySelector('.progress') as HTMLElement;
            if (progress) {
                progressElement.innerHTML = `(Proses restore ${percetage}%)`;
                progress.style.width = percetage + "%";
            }
            if (percetage === 100){
                setTimeout(() => {
                    LoadingTimer('Data berhasil direstore', 'success', 1500);
                }, 1000);
            }
        }
    }, [percetage])

    const handleRestore = async () => {
        if (selectedFile) {
            try {
                const reader = new FileReader();
                reader.onload = async (event) => {
                    if (event.target) {
                        handleClose();
                        showWaitLoading('Mencoba mendecrypt data');
                        const encryptedData = event.target.result as string;
                        const decryptedData = await decryptData(encryptedData);
                        if (!decryptedData.success) {
                            await LoadingTimer('Gagal mendecrypt data', 'error', 1500);
                            console.log(decryptedData.message)
                            return;
                        }

                        if (!Object.keys(decryptedData.data)[0] || Object.keys(decryptedData.data)[0] !== type) {
                            await LoadingTimer('Data tidak valid', 'error', 1500);
                            return;
                        }
                        const reset = await Reset(type);
                        if (!reset.success){
                            await LoadingTimer('Gagal mereset data', 'error', 1500);
                            console.log(reset.message)
                            return;
                        }
                        showWaitPercent(`Mencoba merestore data.`, `<br/><p id="persentasenya">(Mempersiapkan data)</p><br><div class="progress-container" style="background-color: #ffffff"><div class="progress" style="background-color: #03853f"></div></div>`);
                        const upload = await uploadBackup(type, decryptedData.data, setPercentage);
                        if (!upload.success){
                            await LoadingTimer('Gagal merestore data', 'error', 1500);
                            console.log(upload.message)
                            return;
                        }
                        reload();
                        handleClose();
                    }
                };
                reader.readAsText(selectedFile);
            } catch (error) {
                console.error('Error restoring data:', error);
            }
        }
    };

    return (
        <>
            <button
                type="button"
                className="btn btn-warning m-3"
                onClick={handleShow}
            >
                Restore
            </button>

            <Modal show={show} onHide={handleClose} centered={true}>
                <Modal.Header closeButton>
                    <Modal.Title>Restore Backup</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <input type="file" accept=".mazaha" onChange={handleFileChange}/>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleRestore}>
                        Restore
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default RestoreModal;
