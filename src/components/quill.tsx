import React from 'react';

import 'react-quill/dist/quill.snow.css'
import dynamic from 'next/dynamic'

const QuillNoSSRWrapper = dynamic(import('react-quill'), {
    ssr: false,
    loading: () => <p>Loading ...</p>,
})

const toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],
    ['blockquote', 'code-block'],
    [{ 'header': 1 }, { 'header': 2 }],
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'script': 'sub'}, { 'script': 'super' }],
    [{ 'indent': '-1'}, { 'indent': '+1' }],
    [{ 'direction': 'rtl' }],
    [{ 'size': ['small', false, 'large', 'huge'] }],
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'color': [] }, { 'background': [] }],
    [{ 'font': [] }],
    [{ 'align': [] }],
    ['clean']
];

const modules = {
    toolbar: toolbarOptions,
    clipboard: {
        matchVisual: false,
    },
}
const formats = [
    'background',
    'color',
    'font',
    'code',
    'strike',
    'script',
    'align',
    'direction',
    'code-block',
    'header',
    'font',
    'size',
    'bold',
    'italic',
    'underline',
    'blockquote',
    'list',
    'bullet',
    'indent',
    'link',
    'video',
]

interface QuillProps {
    datanya: {
        judul: string;
        setter: React.Dispatch<React.SetStateAction<string>>;
        value: string;
    };
}

const Quill: React.FC<QuillProps> = ({ datanya }) => {
    const { judul, setter, value } = datanya;

    const handleChange = (content: string) => {
        setter(content);
    };

    return (
        <React.Fragment>
            <label>{judul}</label>
            <QuillNoSSRWrapper modules={modules} formats={formats} theme="snow" onChange={handleChange} defaultValue={value} />
        </React.Fragment>
    );
};

export default Quill;