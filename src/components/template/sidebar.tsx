import React, {useState, useEffect} from 'react';
import Link from "next/link";
import {Icon} from '@iconify/react';
import {useSidebarContext} from "@/contexts/sidebar";

const Sidebar = () => {
    const {state, dispatch} = useSidebarContext();
    const [person, setPerson] = useState<boolean>(false);
    const [blog, setBlog] = useState<boolean>(false);

    useEffect(() => {
        switch (state.active) {
            case 'person':
                setPerson(true);
                setBlog(false);
                break;
            case 'blog':
                setBlog(true);
                setPerson(false);
                break;
            default:
                setBlog(false);
                setPerson(false);
        }
    }, [state.active]);
    const handleClick = (typeclick: string) => {
        switch (typeclick) {
            case 'person':
                setPerson(!person)
                break;
            case 'blog':
                setBlog(!blog)
                break;
            default:
                setBlog(false);
                setPerson(false);
        }
    }
    return (
        <aside id="sidebar" className="sidebar">
            <ul className="sidebar-nav" id="sidebar-nav">
                <li className="nav-item" key={'1'}>
                    <Link
                        href={"/"}
                        className={`nav-link ${state.active === 'dashboard' ? '' : 'collapsed'}`}
                        onClick={(e) => {
                            handleClick('dashboard')
                            dispatch({type: 'dashboard'})
                        }}>
                        <i className="bi bi-mortarboard"></i>
                        <span>Dashboard</span>
                    </Link>
                </li>
                <li className="nav-heading">Master Data</li>
                <li className="nav-item" onClick={() => (setPerson(!person))}>
                    <Link
                        className={`nav-link ${state.active === 'person' ? '' : 'collapsed'}`}
                        data-bs-target="#components-nav"
                        data-bs-toggle="collapse"
                        style={{textDecoration: "none"}}
                        aria-expanded={person}
                        href="#">
                        <i className="bi bi-person" style={{textDecoration: "none"}}></i>
                        <span>Data Person</span>
                        <i className="bi bi-chevron-down ms-auto"></i>
                    </Link>
                    <ul id="components-nav"
                        className={`nav-content collapse ${person ? 'show' : ''}`}
                        data-bs-parent="#sidebar-nav">
                        <li>
                            <Link
                                href={"/guru_dan_staff"}
                                className="active"
                                data-bs-target="#components-nav"
                                data-bs-toggle="collapse"
                                onClick={() => (
                                    dispatch({type: 'person'})
                                )}>
                                <Icon icon="gg:check-o" color="green" style={{marginRight: 5}}/>
                                <span>Guru & Staff</span>
                            </Link>
                        </li>
                    </ul>
                </li>
                <li className="nav-heading">Blog</li>
                <li className="nav-item" onClick={() => (handleClick('blog'))}>
                    <Link
                        className={`nav-link ${blog ? '' : 'collapsed'}`}
                        data-bs-target="#blog-nav"
                        data-bs-toggle="collapse"
                        style={{textDecoration: "none"}}
                        aria-expanded={blog}
                        href="#">
                        <i className="bi bi-person" style={{textDecoration: "none"}}></i>
                        <span>Blog Data</span>
                        <i className="bi bi-chevron-down ms-auto"></i>
                    </Link>
                    <ul id="blog-nav"
                        className={`nav-content collapse ${blog ? 'show' : ''}`}
                        data-bs-parent="#sidebar-nav">
                        <li>
                            <Link
                                href={"/blog"}
                                className="active"
                                data-bs-target="#components-nav"
                                data-bs-toggle="collapse"
                                onClick={() => (
                                    dispatch({type: 'blog'})
                                )}>
                                <Icon icon="gg:check-o" color="green" style={{marginRight: 5}}/>
                                <span>Semua Post</span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={"/blog/tambah"}
                                className="active"
                                data-bs-target="#components-nav"
                                data-bs-toggle="collapse"
                                onClick={() => (
                                    dispatch({type: 'blog'})
                                )}>
                                <Icon icon="gg:check-o" color="green" style={{marginRight: 5}}/>
                                <span>Tambah Post</span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={"/blog/kategori"}
                                className="active"
                                data-bs-target="#components-nav"
                                data-bs-toggle="collapse"
                                onClick={() => (
                                    dispatch({type: 'blog'})
                                )}>
                                <Icon icon="gg:check-o" color="green" style={{marginRight: 5}}/>
                                <span>Kategori</span>
                            </Link>
                        </li>
                    </ul>
                </li>
                <li className="nav-heading">Info</li>
                <li className="nav-item">
                    <Link
                        style={{textDecoration: "none"}}
                        href={"/notifikasi"}
                        className={`nav-link ${state.active === 'notifikasi' ? '' : 'collapsed'}`}
                        onClick={(e) => {
                            handleClick('notifikasi')
                            dispatch({type: 'notifikasi'})
                        }}>
                        <i className="bi bi-bell"></i>
                        <span>Buat Notifikasi</span>
                    </Link>
                </li>
                <li className="nav-heading">Setting</li>
                <li className="nav-item">
                    <Link
                        style={{textDecoration: "none"}}
                        href={"/profile"}
                        className={`nav-link ${state.active === 'profile' ? '' : 'collapsed'}`}
                        onClick={(e) => {
                            handleClick('profile')
                            dispatch({type: 'profile'})
                        }}>
                        <i className="bi bi-person"></i>
                        <span>Profile</span>
                    </Link>
                </li>
            </ul>
        </aside>
    );
};

export default Sidebar;