import React from 'react';
import axios from "axios";
import {getCookie, deleteCookie} from "cookies-next";
import Link from "next/link";
import {CldImage} from "next-cloudinary";

const Navbar = () => {
    // console.log(getCookie('token'))
    const [expanded, setExpanded] = React.useState<boolean>(false);
    const [data, setData] = React.useState<any>(null);

    React.useEffect(() => {
        const api = `${process.env.NEXT_PUBLIC_API_URL}/auth/detail` ?? '';
        const token = getCookie('token');
        fetchData(api, token);
    }, []);

    const fetchData = async (api: string, token: string | undefined | null | boolean) => {
        const axs = axios.create({
            headers: {
                'Authorization': 'Bearer ' + token
            }
        });
        axs.post(api).then((res) => {
            setData(res.data.data)
        }).catch((err) => {
            setData(null)
        });
    }

    const logout = async () => {
        deleteCookie('token');
        window.location.href = '/auth';
    }
    return (
        <>
            <nav className="header-nav ms-auto">
                <ul className="d-flex align-items-center">
                    <li className="nav-item d-block d-lg-none">
                    </li>
                    <li className="nav-item dropdown pe-3" onClick={() => (setExpanded(!expanded))}>
                        <a className={`nav-link nav-profile d-flex align-items-center pe-0 ${expanded ? 'show' : ''}`}
                           href="#"
                           data-bs-toggle="dropdown" aria-expanded={expanded}>
                            {data?.gambar ? (
                                <CldImage
                                    width="500"
                                    height="300"
                                    className="rounded-circle"
                                    src={data.gambar}
                                    alt="gambar"
                                    priority
                                    style={{height: 'auto', width: 'auto'}}
                                />
                            ) : (
                                <CldImage
                                    width="500"
                                    height="300"
                                    className="rounded-circle"
                                    src="website/gs/gpbe2jttvnhh1egbw6in"
                                    alt="gambar"
                                    priority
                                    style={{height: 'auto', width: 'auto'}}
                                />
                            )}
                            <span className="d-none d-md-block dropdown-toggle ps-2">{data?.nama ?? '....'}</span>
                        </a>

                        <ul className={`dropdown-menu dropdown-menu-end dropdown-menu-arrow profile ${expanded ? 'show' : ''}`}
                            style={{
                                position: "absolute",
                                inset: "0 0 auto auto",
                                margin: "0",
                                transform: "translate(-16px, 38px)",
                            }}>
                            <li className="dropdown-header">
                                <h6>@{data?.username ?? '....'}</h6>
                                <span>{data?.role ?? '....'}</span>
                            </li>
                            <li>
                                <hr className="dropdown-divider"/>
                            </li>

                            <li>
                                <Link
                                    className="dropdown-item d-flex align-items-center"
                                    style={{textDecoration: "none"}}
                                    href={"/profile"}>
                                    <i className="bi bi-person"></i>
                                    <span>Profile</span>
                                </Link>
                            </li>
                            <li>
                                <hr className="dropdown-divider"/>
                            </li>

                            <li>
                                <hr className="dropdown-divider"/>
                            </li>

                            <li>
                                <button type="button" className="dropdown-item d-flex align-items-center" onClick={logout}>
                                    <i className="bi bi-box-arrow-right"></i>
                                    <span>Logout</span>
                                </button>
                            </li>

                        </ul>
                    </li>

                </ul>
            </nav>
        </>
    );
};

export default Navbar;