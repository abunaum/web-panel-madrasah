import React, { ReactNode } from 'react';

interface AuthProps {
    children: ReactNode;
}

const Auth: React.FC<AuthProps> = ({ children }) => {
    return (
        <>
            <main>
                <div className="container">
                    <section className="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">
                                    {children}
                                    <div className="credits">
                                        Designed by <a href="https://t.me/@abu_naum">Ahmad Yani</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </main>
        </>
    );
};

export default Auth;