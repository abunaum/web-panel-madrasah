import React, {useState} from 'react';
import styles from "@/styles/card_profile.module.css";
import {CldImage} from "next-cloudinary";
import PasswordModal from "@/components/modal/password";
import ProfileModal from "@/components/modal/profile";

type ProfileProps = {
    data: any
}
const Profile: React.FC<ProfileProps> = ({data}) => {
    return (
        <div className={styles.card}>
            <h1 className={styles.text1}>{data.role.toUpperCase()}</h1>
            <div className={styles.image_crop}>
                {data.gambar ? (
                    <CldImage
                        width="900"
                        height="900"
                        className={styles.avatar}
                        src={data.gambar}
                        alt="gambar"
                        priority
                        style={{height: '150px', width: '150px'}}
                    />
                ) : (
                    <CldImage
                        width="1200"
                        height="900"
                        className={styles.avatar}
                        src={"website/gs/gpbe2jttvnhh1egbw6in"}
                        alt="gambar"
                        priority
                        style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                    />
                )}
            </div>
            <div className={styles.bio}>
                <p className={styles.nama}>{data.nama}</p>
                <p>(@{data.username})<br/>{data.email}</p>
            </div>
            <div className={styles.buttons}>
                <PasswordModal />
                <ProfileModal data={data}/>
            </div>
        </div>
    );
};

export default Profile;