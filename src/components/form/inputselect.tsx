import {Path, UseFormRegister} from "react-hook-form";
import React from "react";
import {UCF} from "@/utils/lite_function/UCF";

interface IFormValues {
    nama: string
    jabatan: string
    bidang_studi: string
    jenis: string
    alamat: string
    nohp: string
    image: string
    telegram: string
    instagram: string
    facebook: string
}

type InputProps = {
    label: Path<IFormValues>
    register: UseFormRegister<IFormValues>
    required: boolean,
    placeholder: string,
    defaultValue?: string
}
const Input = ({label, register, required, placeholder, defaultValue}: InputProps) => (
    <>
        <label className="form-label">{placeholder}</label>
        <input {...register(label, {required})} className="form-control" placeholder={placeholder}
               defaultValue={defaultValue}/>
    </>
)

// eslint-disable-next-line react/display-name
const Select = React.forwardRef<
    HTMLSelectElement,
    { label: string, data: string[], defaultValue?: string } & ReturnType<UseFormRegister<IFormValues>>
>(({onChange, onBlur, name, label, data, defaultValue}, ref) => (
    <>
        <label className="form-label">{label}</label>
        <select name={name} ref={ref} onChange={onChange} onBlur={onBlur} className="form-select" defaultValue={defaultValue}>
            {data.map((item, index) => (
                <option key={index} value={item}>
                    {UCF(item)}
                </option>
            ))}
        </select>
    </>
))

export {Input, Select};
export type {IFormValues};
