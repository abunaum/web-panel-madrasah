import React, {createContext, useContext, useReducer} from 'react';

// Initial state
type SidebarState = {
    active: 'dashboard' | 'person' | 'blog' | 'profile' | 'notifikasi';
}

type SidebarAction = {
    type: 'person' | 'blog' | 'dashboard' | 'profile' | 'notifikasi';
}

const initialState: SidebarState = {
    active: 'dashboard',
};
// Action types
const person = 'person';
const blog = 'blog';
const dashboard = 'dashboard';
const profile = 'profile';
const notifikasi = 'notifikasi';

// Reducer function
const sidebarReducer = (state: any, action: SidebarAction) => {
    switch (action.type) {
        case person:
            return {...state, active: 'person'};
        case blog:
            return {...state, active: 'blog'};
        case dashboard:
            return {...state, active: 'dashboard'};
        case profile:
            return {...state, active: 'profile'};
        case notifikasi:
            return {...state, active: 'notifikasi'};
        default:
            return state;
    }
};

// Create context
const SidebarContext = createContext<any>(null);

type SidebarProviderProps = {
    children: React.ReactNode;
}
// Context provider
export const SidebarProvider: React.FC<SidebarProviderProps> = ({children}) => {
    const [state, dispatch] = useReducer(sidebarReducer, initialState);

    return (
        <SidebarContext.Provider value={{state, dispatch}}>
            {children}
        </SidebarContext.Provider>
    );
};

export const useSidebarContext = () => {
    const context = useContext(SidebarContext);
    if (!context) {
        throw new Error('useSidebarContext must be used within a SidebarProvider');
    }
    return context;
};