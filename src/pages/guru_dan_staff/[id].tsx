import React from 'react';
import {useRouter} from "next/router";
import TitlePages from "@/components/title";
import Image from "next/image";
import Link from "next/link";
import Spinner from "@/components/spinner";
import axios from "axios";
import useSWR from "swr";
import {getCookie} from "cookies-next";
import {CldImage} from "next-cloudinary";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import {useForm} from "react-hook-form";
import {IFormValues, Input, Select} from "@/components/form/inputselect";

const req1 = (url: string) => axios.get(url, {
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("token") ?? "",
    }
}).then(res => res.data)
const Person = () => {
    const {register: fdata, handleSubmit} = useForm<IFormValues>();
    let {id} = useRouter().query;
    const api: string = process.env.NEXT_PUBLIC_API_URL ?? "";
    const url: string = `${api}/gs/${id}`;
    const {data, error, isLoading, mutate} = useSWR(id ? url : null, req1);
    const [uploadedImageUrl, setUploadedImageUrl] = React.useState<string | null>(null);
    const nama: string = "Edit Guru & Staff";

    const data_select = ["PIMPINAN", "GURU", "KARYAWAN"];
    const handleImageUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = (e) => {
                setUploadedImageUrl(e.target?.result as string);
            };

            reader.readAsDataURL(file);
        } else {
            setUploadedImageUrl(null);
        }
    }

    const onSubmit = async (data: any) => {
        let gambar: string | null = null;
        console.log("edit di klik")
        if (uploadedImageUrl) {
            console.log("upload gambar")
            showWaitLoading('Mengupload gambar.')
            const formData = new FormData();
            const file = document.getElementById("foto") as HTMLInputElement;
            formData.append("file", file.files?.[0] as Blob);
            formData.append("location", "gs");
            if (file.files?.[0]) {
                try {
                    const upload = await axios.post('/api/upload', formData, {
                        headers: {
                            "Content-Type": "multipart/form-data",
                        }
                    });
                    if (upload.data.success) {
                        await LoadingTimer('Berhasil mengupload gambar.', 'success', 1500);
                        gambar = upload.data.data.public_id;
                    } else {
                        await LoadingTimer('Gagal mengupload gambar.', 'error', 1500);
                        console.log(upload.data.message);
                        return;
                    }
                } catch (e) {
                    await LoadingTimer('Gagal mengupload gambar.', 'error', 1500);
                    console.log(e);
                    return;
                }
            }
        }
        const new_data = {
            nama: data.nama,
            jabatan: data.jabatan,
            bidang_studi: data.bidang_studi,
            jenis: data.jenis,
            alamat: data.alamat,
            no_hp: data.nohp,
            profile: {
                image: gambar,
                telegram: data.telegram,
                instagram: data.instagram,
                facebook: data.facebook,
            }
        }
        await proses_edit(new_data, id, mutate);
    }
    return (
        <>
            <TitlePages head={"Data Person"} nama={nama}>
                {isLoading && (
                    <div id="loading" className="loading-container">
                        <Spinner text={"Megambil Data."}/>
                    </div>
                )}
                {error && (
                    <div id="loading" className="loading-container">
                        <h1>Terjadi Kesalahan</h1>
                    </div>
                )}
                {data ? (
                    <section className="section dashboard">
                        <div className="row">
                            <div className="col-lg-12">
                                <div style={{textAlign: "center"}}>
                                    <h1 id="judulEdit">Edit {data.nama}</h1>
                                </div>
                                <form className="row g-3" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="col-md-12">
                                        <Input
                                            label="nama"
                                            register={fdata}
                                            required={true}
                                            placeholder="Nama"
                                            defaultValue={data.nama ?? ""}/>
                                    </div>
                                    <div className="col-md-4">
                                        <Input
                                            label="jabatan"
                                            register={fdata}
                                            required={true}
                                            placeholder="Jabatan"
                                            defaultValue={data.jabatan ?? ""}/>
                                    </div>
                                    <div className="col-md-4">
                                        <Input
                                            label="bidang_studi"
                                            register={fdata}
                                            required={false}
                                            placeholder="Bidang Studi"
                                            defaultValue={data.bidang_studi ?? ""}/>
                                    </div>
                                    <div className="col-md-4">
                                        <Select label="Jenis" data={data_select} defaultValue={data.jenis} {...fdata("jenis")} />
                                    </div>
                                    <div className="col-md-6">
                                        <Input
                                            label="alamat"
                                            register={fdata}
                                            required={false}
                                            placeholder="Alamat"
                                            defaultValue={data.alamat ?? ""}/>
                                    </div>
                                    <div className="col-md-6">
                                        <Input
                                            label="nohp"
                                            register={fdata}
                                            required={false}
                                            placeholder="No HP"
                                            defaultValue={data.no_hp ?? ""}/>
                                    </div>
                                    <div className="col-md-4">
                                        <Input
                                            label="telegram"
                                            register={fdata}
                                            required={false}
                                            placeholder="Username Telegram"
                                            defaultValue={data.profile?.telegram ?? ""}/>
                                    </div>
                                    <div className="col-md-4">
                                        <Input
                                            label="instagram"
                                            register={fdata}
                                            required={false}
                                            placeholder="Username Instagram"
                                            defaultValue={data.profile?.instagram ?? ""}/>
                                    </div>
                                    <div className="col-md-4">
                                        <Input
                                            label="facebook"
                                            register={fdata}
                                            required={false}
                                            placeholder="Username Facebook"
                                            defaultValue={data.profile?.facebook ?? ""}/>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="foto" className="form-label">Gambar</label>
                                        <input className="form-control mb-2" type="file" id="foto" name="foto"
                                               onChange={async (e) => {
                                                   await handleImageUpload(e)
                                               }}/>
                                        {!uploadedImageUrl && data.profile?.image && (
                                            <CldImage
                                                width="1200"
                                                height="900"
                                                className="img-thumbnail"
                                                src={data.profile.image}
                                                alt="gambar"
                                                priority
                                                style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                            />
                                        )}
                                        {!uploadedImageUrl && !data.profile?.image && (
                                            <CldImage
                                                width="1200"
                                                height="900"
                                                className="img-thumbnail"
                                                src={"website/gs/gpbe2jttvnhh1egbw6in"}
                                                alt="gambar"
                                                priority
                                                style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                            />
                                        )}
                                        {uploadedImageUrl && (
                                            <Image
                                                id={"image-preview"}
                                                src={uploadedImageUrl}
                                                alt="gambar"
                                                className={"img-thumbnail"}
                                                width="1200"
                                                height="900"
                                                priority
                                                style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                            />
                                        )}
                                    </div>
                                    <div className="text-center">
                                        <Link
                                            href={{
                                                pathname: '/guru_dan_staff',
                                            }}
                                            className="btn btn-warning"
                                            style={{textDecoration: "none", marginRight: "5px"}}
                                        >
                                            Kembali
                                        </Link>
                                        <button type="submit" className="btn btn-success">
                                            Edit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                ) : (
                    <center>
                        <section className="section dashboard">
                            <h1>Data tidak ditemukan</h1>
                        </section>
                    </center>
                )}
            </TitlePages>
        </>
    );
};

export default Person;

const proses_edit = async (data: any, id: string | string[] | undefined, mutate: any) => {
    const fix_data = await removeEmptyOrNullValues(data);
    const api: string = process.env.NEXT_PUBLIC_API_URL ?? "";
    const url: string = `${api}/gs/edit/${id}`;
    const token: string | boolean = getCookie("token") ?? "";
    showWaitLoading('Mengedit data.')
    try {
        const edit = await axios.post(url, fix_data, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token,
            }
        });
        if (edit.data.success) {
            await LoadingTimer('Berhasil mengedit data.', 'success', 1500);
            await mutate();
        } else {
            await LoadingTimer('Gagal mengedit data.', 'error', 1500);
            console.log(edit.data.message);
            await mutate();
            return;
        }
    } catch (e) {
        await LoadingTimer('Gagal mengedit data.', 'error', 1500);
        console.log(e);
        await mutate();
        return;
    }
}
const removeEmptyOrNullValues = (obj: any) => {
    for (const key in obj) {
        if (obj[key] === "" || obj[key] === null) {
            delete obj[key];
        } else if (typeof obj[key] === "object") {
            obj[key] = removeEmptyOrNullValues(obj[key]);
        }
    }
    return obj;
}