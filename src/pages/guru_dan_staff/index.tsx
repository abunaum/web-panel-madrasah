import React from 'react';
import TitlePages from "@/components/title";
import Table from "@/components/table"
import {CookieValueTypes, getCookie} from "cookies-next";
import axios from "axios";
import {backupData} from "@/utils/backup";
import {encryptData} from "@/utils/encrypt";
import Link from "next/link";
import RestoreModal from '@/components/modal/restore';
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";

const GS = () => {
    const [tablewiew, setTablewiew] = React.useState<boolean>(true);
    const head: { name: string, id: string }[] = [
        {
            name: "#",
            id: "nomor_urut",
        },
        {
            name: "Nama",
            id: "nama",
        },
        {
            name: "Jabatan",
            id: "jabatan",
        },
        {
            name: "Bidang Studi",
            id: "bidang_studi",
        },
        {
            name: "No Hp",
            id: "no_hp",
        }
    ];

    const reload = async() => {
        setTablewiew(false);
        await new Promise(resolve => setTimeout(resolve, 500));
        setTablewiew(true);
    }
    const backup = async () => {
        const token: CookieValueTypes = getCookie("token");
        const api: string = process.env.NEXT_PUBLIC_API_URL ?? "";
        showWaitLoading('Mencoba mendownload backup data.')
        try {
            const res = await axios.post(`${api}/gs/backup`, {}, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`,
                }
            });
            if (res.data.success) {
                const filename: string = 'gs';
                const fixdata: string = encryptData(filename, res.data.data);
                backupData(filename, fixdata);
                await LoadingTimer('Berhasil mendownload data backup.', 'success', 1500);
            } else {
                await LoadingTimer('Gagal mendownload data backup.', 'error', 1500);
                console.log(res.data.message);
            }
        } catch (e) {
            await LoadingTimer('Gagal mendownload data backup.', 'error', 1500);
            console.log(e);
        }
    }
    const aksi= [
        {
            name: "Edit",
            url: "/guru_dan_staff",
        },
        {
            name: "Hapus",
            url :"/gs",
        }
    ];
    const url: string = "/gs";
    const nama: string = "Guru & Staff";
    return (
        <>
            <TitlePages head={"Data Person"} nama={nama}>
                <section className="section dashboard">
                    <div className="row">
                        <div className="col-lg-12">
                            {tablewiew && (
                                <Table data={{head, aksi, url, nama}}/>
                            )}
                            <center>
                                <button className="btn btn-primary m-3" onClick={backup}>Backup</button>
                                <RestoreModal type="gs" reload={reload}/>
                                <Link
                                    href={"/guru_dan_staff/tambah"}
                                    className="btn btn-success m-3">
                                    Tambah
                                </Link>
                            </center>
                        </div>
                    </div>
                </section>
            </TitlePages>
        </>
    );
};
export default GS;