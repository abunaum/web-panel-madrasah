import React from 'react';
import TitlePages from "@/components/title";
import Image from "next/image";
import Link from "next/link";
import axios from "axios";
import {getCookie} from "cookies-next";
import {CldImage} from "next-cloudinary";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import { Input, Select } from '@/components/form/inputselect';
import { IFormValues} from "@/components/form/inputselect";
import {useForm} from "react-hook-form";


const Person = () => {
    const {register: data, handleSubmit} = useForm<IFormValues>();
    const [uploadedImageUrl, setUploadedImageUrl] = React.useState<string | null>(null);
    const nama: string = "Tambah Guru & Staff";
    const data_select = ["PIMPINAN", "GURU", "KARYAWAN"];
    const handleImageUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = (e) => {
                setUploadedImageUrl(e.target?.result as string);
            };

            reader.readAsDataURL(file);
        } else {
            setUploadedImageUrl(null);
        }
    };

    const onSubmit = async (data: any) => {
        let gambar;
        if (uploadedImageUrl) {
            showWaitLoading('Mengupload gambar.')
            const formData = new FormData();
            const file = document.getElementById("foto") as HTMLInputElement;
            formData.append("file", file.files?.[0] as Blob);
            formData.append("location", "gs");
            if (file.files?.[0]) {
                try {
                    const upload = await axios.post('/api/upload', formData, {
                        headers: {
                            "Content-Type": "multipart/form-data",
                        }
                    });
                    if (upload.data.success) {
                        await LoadingTimer('Berhasil mengupload gambar.', 'success', 1500);
                        gambar = upload.data.data.public_id;
                    } else {
                        await LoadingTimer('Gagal mengupload gambar.', 'error', 1500);
                        console.log(upload.data.message);
                        return;
                    }
                } catch (e) {
                    await LoadingTimer('Gagal mengupload gambar.', 'error', 1500);
                    console.log(e);
                    return;
                }
            }
        } else {
            gambar = "website/gs/gpbe2jttvnhh1egbw6in";
        }
        const new_data = {
            nama: data.nama,
            jabatan: data.jabatan,
            bidang_studi: data.bidang_studi,
            jenis: data.jenis,
            alamat: data.alamat,
            no_hp: data.nohp,
            profile: {
                image: gambar,
                telegram: data.telegram,
                instagram: data.instagram,
                facebook: data.facebook,
            }
        }
        await proses_tambah(new_data);
    }
    return (
        <>
            <TitlePages head={"Data Person"} nama={nama}>
                <section className="section dashboard">
                    <div className="row">
                        <div className="col-lg-12">
                            <div style={{textAlign: "center"}}>
                                <h1 id="judulEdit">Tambah Guru dan Staff</h1>
                            </div>
                            <form className="row g-3" onSubmit={handleSubmit(onSubmit)}>
                                <div className="col-md-12">
                                    <Input
                                        label="nama"
                                        register={data}
                                        required={true}
                                        placeholder="Nama"/>
                                </div>
                                <div className="col-md-4">
                                    <Input
                                        label="jabatan"
                                        register={data}
                                        required={true}
                                        placeholder="Jabatan"/>
                                </div>
                                <div className="col-md-4">
                                    <Input
                                        label="bidang_studi"
                                        register={data}
                                        required={false}
                                        placeholder="Bidang Studi"/>
                                </div>
                                <div className="col-md-4">
                                    <Select label="Jenis" data={data_select} {...data("jenis")} />
                                </div>
                                <div className="col-md-6">
                                    <Input
                                        label="alamat"
                                        register={data}
                                        required={false}
                                        placeholder="Alamat"/>
                                </div>
                                <div className="col-md-6">
                                    <Input
                                        label="nohp"
                                        register={data}
                                        required={false}
                                        placeholder="No HP"/>
                                </div>
                                <div className="col-md-4">
                                    <Input
                                        label="telegram"
                                        register={data}
                                        required={false}
                                        placeholder="Username Telegram"/>
                                </div>
                                <div className="col-md-4">
                                    <Input
                                        label="instagram"
                                        register={data}
                                        required={false}
                                        placeholder="Username Instagram"/>
                                </div>
                                <div className="col-md-4">
                                    <Input
                                        label="facebook"
                                        register={data}
                                        required={false}
                                        placeholder="Username Facebook"/>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="foto" className="form-label">Gambar</label>
                                    <input className="form-control mb-2" type="file" id="foto" name="foto"
                                           onChange={async (e) => {
                                               await handleImageUpload(e)
                                           }}/>
                                    {uploadedImageUrl ? (
                                        <Image
                                            id={"image-preview"}
                                            src={uploadedImageUrl}
                                            alt="gambar"
                                            className={"img-thumbnail"}
                                            width="1200"
                                            height="900"
                                            priority
                                            style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                        />
                                    ) : (
                                        <CldImage
                                            width="1200"
                                            height="900"
                                            className="img-thumbnail"
                                            src={"website/gs/gpbe2jttvnhh1egbw6in"}
                                            alt="gambar"
                                            priority
                                            style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                        />
                                    )}
                                </div>
                                <div className="text-center">
                                    <Link
                                        href={{
                                            pathname: '/guru_dan_staff',
                                        }}
                                        className="btn btn-warning"
                                        style={{textDecoration: "none", marginRight: "5px"}}
                                    >
                                        Kembali
                                    </Link>
                                    <button type="submit" className="btn btn-success">Tambah</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </TitlePages>
        </>
    );
};

export default Person;

const proses_tambah = async (data: any) => {
    const fix_data = await removeEmptyOrNullValues(data);
    const api: string = process.env.NEXT_PUBLIC_API_URL ?? "";
    const url: string = `${api}/gs/tambah`;
    const token: string | boolean = getCookie("token") ?? "";
    showWaitLoading('Mengedit data.')
    try {
        const edit = await axios.post(url, fix_data, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token,
            }
        });
        if (edit.data.success) {
            await LoadingTimer('Berhasil menambah data.', 'success', 1500);
            window.location.href = "/guru_dan_staff";
        } else {
            await LoadingTimer('Gagal menambah data.', 'error', 1500)
            console.log(edit.data.message);
            return;
        }
    } catch (e) {
        await LoadingTimer('Gagal menambah data.', 'error', 1500)
        console.log(e);
        return;
    }
}
const removeEmptyOrNullValues = (obj: any) => {
    for (const key in obj) {
        if (obj[key] === "" || obj[key] === null) {
            delete obj[key];
        } else if (typeof obj[key] === "object") {
            obj[key] = removeEmptyOrNullValues(obj[key]);
        }
    }
    return obj;
}