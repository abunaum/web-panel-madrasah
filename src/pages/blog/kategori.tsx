import React from 'react';
import TitlePages from "@/components/title";
import Table from "@/components/table";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, Form} from "reactstrap";
import axios from "axios";
import {getCookie} from "cookies-next";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";

const GS = () => {
    const head: { name: string, id: string }[] = [
        {
            name: "#",
            id: "nomor_urut",
        },
        {
            name: "Nama",
            id: "nama",
        }
    ];
    const [modalTambah, setModalTambah] = React.useState<boolean>(false);
    const [name, setName] = React.useState<string>("");
    const [reloadTableKey, setReloadTableKey] = React.useState<number>(0);
    const aksi= [
        {
            name: "Hapus",
            url :"/kategori",
        }
    ];
    const url = "/kategori";
    const nama = "Kategori";
    const toggleModalTambah = () => {
        setModalTambah(!modalTambah);
    };
    const handleSimpan = async () => {
        setModalTambah(false)
        if (name === "") {
            await LoadingTimer('Nama tidak boleh kosong', 'error', 1500);
        } else {
            const data: { nama: string } = {
                nama: name
            }
            showWaitLoading('Mencoba menyimpan data.')
            try {
                const token = getCookie("token");
                const save = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/kategori/tambah`, data, {
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token ?? "",
                    }
                });
                if (save.data.success) {
                    LoadingTimer('Berhasil menyimpan data.', 'success', 1500);
                    setName("")
                    setReloadTableKey(prevKey => prevKey + 1);
                } else {
                    LoadingTimer('Gagal menyimpan data.', 'error', 1500);
                    setName("")
                    console.log(save.data)
                }
            } catch (e){
                LoadingTimer('Gagal menyimpan data.', 'error', 1500);
                setName("")
                console.log(e)
            }
        }

    }
    return (
        <>
            <TitlePages head={"Blog"} nama={nama}>
                <section className="section dashboard">
                    <div className="row">
                        <div className="col-lg-12">
                            <Table data={{head, aksi, url, nama}} key={reloadTableKey}/>
                            <center>
                                <Button color="success" onClick={toggleModalTambah}>
                                    Tambah Kategori
                                </Button>
                                <Modal isOpen={modalTambah} toggle={toggleModalTambah} size={"m"}>
                                    <ModalHeader toggle={toggleModalTambah}>Judul Modal</ModalHeader>
                                    <ModalBody>
                                        <Form>
                                            <div className="mb-3">
                                                <label htmlFor="nama" className="form-label">Nama</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    id="nama"
                                                    placeholder="Nama"
                                                    value={name}
                                                    onChange={(event) => setName(event.target.value)}
                                                />
                                            </div>
                                        </Form>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="danger" onClick={toggleModalTambah}>
                                            Batal
                                        </Button>
                                        <Button color="success" onClick={handleSimpan}>
                                            Simpan
                                        </Button>
                                    </ModalFooter>
                                </Modal>
                            </center>
                        </div>
                    </div>
                </section>
            </TitlePages>
        </>
    );
};

export default GS;