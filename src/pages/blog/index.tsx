import React from 'react';
import Link from "next/link";
import TitlePages from "@/components/title";
import Table from "@/components/table"
import {getCookie} from "cookies-next";
import axios from "axios";
import 'moment/locale/id';
import {backupData} from "@/utils/backup";
import {encryptData} from "@/utils/encrypt";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import RestoreModal from "@/components/modal/restore";

const Post = () => {
        const [tablewiew, setTablewiew] = React.useState<boolean>(true);
        const head: { name: string, id: string }[] = [
            {
                name: "#",
                id: "nomor_urut",
            },
            {
                name: "Tanggal",
                id: "created_at",
            },
            {
                name: "Judul",
                id: "judul",
            },
            {
                name: "Kategori",
                id: "kategori",
            }
        ];
        const aksi: any[] = [
            {
                name: "Edit",
                url: "/blog",
            },
            {
                name: "Hapus",
                url: "/post",
            }
        ];
        const url = "/post";
        const nama = "Semua Post";
        const backup = async () => {
            const token = getCookie("token");
            const api = process.env.NEXT_PUBLIC_API_URL;
            showWaitLoading('Mencoba mendownload backup data.')
            try {
                const res = await axios.post(`${api}/post/backup`, {}, {
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${token}`,
                    }
                });
                if (res.data.success) {
                    const filename: string = 'post';
                    const fixdata: string = encryptData(filename, res.data.data);
                    backupData(filename, fixdata);
                    await LoadingTimer('Berhasil mendownload data backup.', 'success', 1500);
                } else {
                    await LoadingTimer('Gagal mendownload data backup.', 'error', 1500);
                    console.log(res.data.message);
                }
            } catch (e) {
                await LoadingTimer('Gagal mendownload data backup.', 'error', 1500);
                console.log(e);
            }
        }
        const reload = async () => {
            setTablewiew(false);
            await new Promise(resolve => setTimeout(resolve, 500));
            setTablewiew(true);
        }
        return (
            <>
                <TitlePages head={"Blog"} nama={nama}>
                    <section className="section dashboard">
                        <div className="row">
                            <div className="col-lg-12">
                                {tablewiew && (
                                    <Table data={{head, aksi, url, nama}}/>
                                )}
                                <center>
                                    <button className="btn btn-primary m-3" onClick={backup}>Backup</button>
                                    <RestoreModal type="post" reload={reload}/>
                                    <Link
                                        href={"/blog/tambah"}
                                        className="btn btn-success m-3">
                                        Tambah
                                    </Link>
                                </center>
                            </div>
                        </div>
                    </section>
                </TitlePages>
            </>
        );
    }
;
export default Post;