import React from 'react';
import TitlePages from "@/components/title";
import Quill from "@/components/quill";
import Image from "next/image";
import {CldImage} from "next-cloudinary";
import Spinner from "@/components/spinner";
import axios from "axios";
import useSWR from "swr";
import {getCookie} from "cookies-next";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import createNotication from "@/utils/OneSignal";
const req1 = (url: string) => axios.get(url, {
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("token") ?? "",
    }
}).then(res => res.data)
const Tambah = () => {
    const api: string = process.env.NEXT_PUBLIC_API_URL ?? "";
    const urlkat: string = `${api}/kategori/semua`;
    const {data: kategori, error: errorkat, isLoading: loadingkat} = useSWR(urlkat, req1)
    const [body, setBody] = React.useState<string>("");
    const [uploadedImageUrl, setUploadedImageUrl] = React.useState<string | null>(null);
    const nama = "Tambah Postingan";

    const handleImageUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = (e) => {
                setUploadedImageUrl(e.target?.result as string);
            };

            reader.readAsDataURL(file);
        } else {
            setUploadedImageUrl(null);
        }
    };
    const handlePost = async () => {
        let gambar: string | null | undefined;
        if (uploadedImageUrl) {
            showWaitLoading('Mengupload gambar.')
            const formData = new FormData();
            const file = document.getElementById("foto") as HTMLInputElement;
            formData.append("file", file.files?.[0] as Blob);
            formData.append("location", "post");
            if (file.files?.[0]) {
                try {
                    const upload = await axios.post('/api/upload', formData, {
                        headers: {
                            "Content-Type": "multipart/form-data",
                        }
                    });
                    if (upload.data.success) {
                        await LoadingTimer('Berhasil mengupload gambar.', 'success', 1500);
                        gambar = upload.data.data.public_id;
                    } else {
                        await LoadingTimer('Gagal mengupload gambar.', 'error', 1500);
                        console.log(upload.data.message);
                        return;
                    }
                } catch (e) {
                    await LoadingTimer('Gagal mengupload gambar.', 'error', 1500);
                    console.log(e);
                    return;
                }
            }
        } else {
            gambar = "website/post/nvslram21uuo8cdlyy0g";
        }
        const data = {
            judul: (document.getElementById("judul") as HTMLInputElement).value,
            slug: (document.getElementById("slug") as HTMLInputElement).value,
            body: body,
            kategori: (document.getElementById("kategori") as HTMLInputElement).value,
            excerpt: `${body.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 350)}...`,
            time: (document.getElementById("time") as HTMLInputElement).value,
            gambar: gambar,
        }
        await tambahPost(data);
    }
    const handleSlug = async (judul: string) => {
        if (judul === "") {
            (document.getElementById("slug") as HTMLInputElement).value = "";
            return;
        } else {
            try {
                const slug = await axios.post(`${api}/api/post/slug`, {judul: judul});
                (document.getElementById("slug") as HTMLInputElement).value = slug.data.slug;
            } catch (e) {
                console.log(e);
            }
        }
    }
    return (
        <TitlePages head={"Blog"} nama={nama}>
            <section className="section dashboard">
                <div className="row">
                    <div className="col-lg-12">
                        {loadingkat && (
                            <div className="text-center">
                                <Spinner text={"Mengambil data kategori."}/>
                            </div>
                        )}
                        {errorkat && (
                            <div className="text-center">
                                <h3 className="text-danger">Gagal mengambil data kategori.</h3>
                            </div>
                        )}
                        {kategori && (
                            <form>
                                <div className="form-group mb-3">
                                    <label htmlFor="judul">Judul</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="judul"
                                        name="judul"
                                        autoFocus
                                        onChange={async (e) => {
                                            await handleSlug(e.target.value)
                                        }}
                                    />
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="slug">Slug</label>
                                    <input type="text" className="form-control" id="slug" name="slug"/>
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="kategori">Kategori</label>
                                    <select className="form-control" id="kategori" name="kategori">
                                        {kategori.data.map((item: any, index: number) => (
                                            <option key={index} value={item.nama}>{item.nama}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group mb-3">
                                    <Quill datanya={{judul: "Body", setter: setBody, value: ""}}/>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="foto" className="form-label">Gambar</label>
                                    <input className="form-control mb-2" type="file" id="foto" name="foto"
                                           onChange={async (e) => {
                                               await handleImageUpload(e)
                                           }}/>
                                    {uploadedImageUrl ? (
                                        <Image
                                            id={"image-preview"}
                                            src={uploadedImageUrl}
                                            alt="gambar"
                                            className={"img-thumbnail mb-3"}
                                            width="1200"
                                            height="900"
                                            priority
                                            style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                        />
                                    ) : (
                                        <CldImage
                                            width="1200"
                                            height="900"
                                            className="img-thumbnail mb-3"
                                            src={"website/post/gpbe2jttvnhh1egbw6in"}
                                            alt="gambar"
                                            priority
                                            style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                        />
                                    )}
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="time">Waktu</label>
                                    <input type="date"
                                           className="form-control"
                                           id="time"
                                           name="time"
                                           defaultValue={new Date().toISOString().split('T')[0]}/>
                                </div>
                                <button type="button" className="btn btn-primary" onClick={handlePost}>Buat Post
                                </button>
                            </form>
                        )}
                    </div>
                </div>
            </section>
        </TitlePages>
    );
};

export default Tambah;

const tambahPost = async (data: any) => {
    showWaitLoading('Membuat post.')
    const api: string = process.env.NEXT_PUBLIC_API_URL ?? "";
    const token = getCookie("token");
    try {
        const upload = await axios.post(`${api}/post/tambah`, data, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            }
        });
        if (upload.data.success) {
            await LoadingTimer('Berhasil membuat post.', 'success', 1500);
            const new_data = {
                judul: data.judul,
                isi: `${data.body.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 100)}...`,
                gambar: `https://res.cloudinary.com/dfko3mdsp/image/upload/${data.gambar}`,
                tujuan: `https://mazainulhasan1.sch.id/berita/${data.slug}`,
            }
            const kirim = await createNotication(new_data);
            if (kirim.success) {
                await LoadingTimer('Berhasil upload dan mengirim notifikasi.', 'success', 1500);
            }
            if (kirim.data.errors) {
                await LoadingTimer('Berhasil upload tapi Gagal mengirim notifikasi.', 'error', 1500);
                await axios.post("/api/telegram", {pesan: kirim.data.errors});
                return
            }
            window.location.href = "/blog";
        } else {
            await LoadingTimer('Gagal membuat post.', 'error', 1500);
            await axios.post("/api/telegram", {pesan: upload});
            return;
        }
    } catch (e) {
        await LoadingTimer('Gagal membuat post.', 'error', 1500);
        await axios.post("/api/telegram", {pesan: e});
        return;
    }
}