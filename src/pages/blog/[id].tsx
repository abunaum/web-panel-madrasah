import React from 'react';
import {useRouter} from "next/router";
import TitlePages from "@/components/title";
import Image from "next/image";
import Spinner from "@/components/spinner";
import axios from "axios";
import useSWR from "swr";
import {getCookie} from "cookies-next";
import {CldImage} from "next-cloudinary";
import Quill from "@/components/quill";
import {EditPost} from "@/utils/post";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
const req1 = (url: string) => axios.get(url, {
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + getCookie("token") ?? "",
    }
}).then(res => res.data)
const Edit = () => {
    const q = useRouter().query;
    let id: string;
    if (!q) {
        id = "";
    } else {
        id = q.id as string;
    }
    const api: string = process.env.NEXT_PUBLIC_API_URL ?? "";
    const url: string = `${api}/post/${id}`;
    const urlkat: string = `${api}/kategori/semua`;
    const {data, error: errordata, isLoading: loadingdata} = useSWR(url, req1);
    const {data: kategori, error: errorkat} = useSWR(urlkat, req1);
    const [body, setBody] = React.useState<string>("");
    const [uploadedImageUrl, setUploadedImageUrl] = React.useState<string | null>(null);
    const nama: string = "Edit Postingan";

    if (errorkat) {
        LoadingTimer('Mengambil data kategori', 'error', 1500);
    }
    const handleImageUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = (e) => {
                setUploadedImageUrl(e.target?.result as string);
            };

            reader.readAsDataURL(file);
        } else {
            setUploadedImageUrl(null);
        }
    };
    const handleSlug = async (judul: string) => {
        if (judul === "") {
            (document.getElementById("slug") as HTMLInputElement).value = "";
            return;
        }
        if (judul === data.data.judul) {
            (document.getElementById("slug") as HTMLInputElement).value = data.data.slug;
            return;

        } else {
            try {
                const slug = await axios.post(`${api}/slug`, {judul: judul});
                (document.getElementById("slug") as HTMLInputElement).value = slug.data.slug;
            } catch (e) {
                console.log(e);
            }
        }
    }
    const editPost = async () => {
        const excerpt = body.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 350);
        const pure_excerpt = excerpt.replace(/\s+/g, " ");
        if (pure_excerpt === "") {
            await LoadingTimer('Body terditeksi kosong, Harap edit body dulu', 'error', 1500);
            return;
        }
        showWaitLoading('Mengedit data')
        type mydata = {
            [key: string]: string|null|undefined;
        };
        let data: mydata = {
            judul: (document.getElementById("judul") as HTMLInputElement).value,
            slug: (document.getElementById("slug") as HTMLInputElement).value,
            kategori: (document.getElementById("kategori") as HTMLInputElement).value,
            excerpt: `${excerpt}...`,
            body: body,
            time: (document.getElementById("time") as HTMLInputElement).value,
        }
        if (uploadedImageUrl) {
            showWaitLoading('Mengupload gambar.')
            const formData = new FormData();
            const file = document.getElementById("foto") as HTMLInputElement;
            formData.append("file", file.files?.[0] as Blob);
            formData.append("location", "post");
            if (file.files?.[0]) {
                try {
                    const upload = await axios.post('/api/upload', formData, {
                        headers: {
                            "Content-Type": "multipart/form-data",
                        }
                    });
                    if (upload.data.success) {
                        data.gambar = upload.data.data.public_id;
                    } else {
                        await LoadingTimer('Gagal mengupload gambar', 'error', 1500);
                        console.log(upload.data.message);
                        return;
                    }
                } catch (e) {
                    await LoadingTimer('Gagal mengupload gambar', 'error', 1500);
                    console.log(e);
                    return;
                }
            }
        } else {
            data.gambar = null;
        }
        showWaitLoading('Mengedit data.')
        const edit_data = await EditPost(data, id);
        if (edit_data.success) {
            await LoadingTimer('Berhasil mengedit data', 'success', 1500);
            window.location.href = "/blog";
        } else {
            await LoadingTimer('Gagal mengedit data', 'error', 1500);
        }
    }
    return (
        <TitlePages head={"Blog"} nama={nama}>
            <section className="section dashboard">
                <div className="row">
                    <div className="col-lg-12">
                        {loadingdata && (
                            <div className="text-center">
                                <Spinner text={"Mengambil data kategori."}/>
                            </div>
                        )}
                        {errordata && (
                            <div className="text-center">
                                <h3 className="text-danger">Gagal mengambil data.</h3>
                            </div>
                        )}
                        {data && (
                            <form>
                                <div className="form-group mb-3">
                                    <label htmlFor="judul">Judul</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="judul"
                                        name="judul"
                                        defaultValue={data.judul}
                                        autoFocus
                                        onChange={async (e) => {
                                            await handleSlug(e.target.value)
                                        }}
                                    />
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="slug">Slug</label>
                                    <input type="text" className="form-control" id="slug" name="slug"
                                           defaultValue={data.slug}/>
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="kategori">Kategori</label>
                                    <select className="form-control" id="kategori" name="kategori"
                                            defaultValue={data.kategori}>
                                        {kategori && kategori.data.map((item: any, index: number) => (
                                            <option key={index} value={item.nama}>{item.nama}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group mb-3">
                                    <Quill datanya={{judul: "Body", setter: setBody, value: data.body}}/>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="foto" className="form-label">Gambar</label>
                                    <input className="form-control mb-2" type="file" id="foto" name="foto"
                                           onChange={async (e) => {
                                               await handleImageUpload(e)
                                           }}/>
                                    {!uploadedImageUrl && data.gambar && (
                                        <CldImage
                                            width="1200"
                                            height="900"
                                            className="img-thumbnail"
                                            src={data.gambar}
                                            alt="gambar"
                                            priority
                                            style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                        />
                                    )}
                                    {!uploadedImageUrl && !data.gambar && (
                                        <CldImage
                                            width="1200"
                                            height="900"
                                            className="img-thumbnail"
                                            src={"website/post/nvslram21uuo8cdlyy0g"}
                                            alt="gambar"
                                            priority
                                            style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                        />
                                    )}
                                    {uploadedImageUrl && (
                                        <Image
                                            id={"image-preview"}
                                            src={uploadedImageUrl}
                                            alt="gambar"
                                            className={"img-thumbnail"}
                                            width="1200"
                                            height="900"
                                            priority
                                            style={{maxWidth: "200px", height: 'auto', width: 'auto'}}
                                        />
                                    )}
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="time">Waktu</label>
                                    <input type="date"
                                           className="form-control"
                                           id="time"
                                           name="time"
                                           defaultValue={new Date(data.created_at).toISOString().split('T')[0]}/>
                                </div>
                                <center>
                                    <button type="button" className="btn btn-primary" onClick={editPost}>Edit Post</button>
                                </center>
                            </form>
                        )}
                    </div>
                </div>
            </section>
        </TitlePages>
    );
};

export default Edit;