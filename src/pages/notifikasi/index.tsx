import React from 'react';
import TitlePages from "@/components/title";
import {notifState} from "@/utils/state/notif";
import validUrl from 'valid-url';
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import createNotication from "@/utils/OneSignal";
import axios from "axios";

const Notifikasi = () => {
    const {judul, isi, gambar, tujuan, setJudul, setIsi, setGambar, setTujuan} = notifState();
    const nama = "Buat Notifikasi";

    const validations: { field: string | undefined, message: string }[] = [
        {field: judul, message: 'Judul masih kosong'},
        {field: isi, message: 'Isi masih kosong'},
        {field: gambar, message: 'URL gambar masih kosong'},
        {field: tujuan, message: 'URL tujuan masih kosong'},
        {field: validUrl.isUri(gambar), message: 'URL gambar tidak valid'},
        {field: validUrl.isUri(tujuan), message: 'URL tujuan tidak valid'},
    ];

    const handleNotif = async () => {
        for (const {field, message} of validations) {
            if (typeof field === 'string' && field === '') {
                await LoadingTimer(message, "error", 1500);
                return;
            } else if (!field) {
                await LoadingTimer(message, "error", 1500);
                return;
            }
        }
        const data = {
            judul,
            isi,
            gambar,
            tujuan
        }
        showWaitLoading("Mencoba mengirim notifikasi");
        const kirim = await createNotication(data);
        if (!kirim.success) {
            await axios.post("/api/telegram", {pesan: kirim.error});
            console.log(kirim.error);
            await LoadingTimer("Gagal mengirim notifikasi", "error", 1500);
            return
        }
        if (kirim.data.errors) {
            await axios.post("/api/telegram", {pesan: kirim.data.errors});
            await LoadingTimer("Gagal mengirim notifikasi", "error", 1500);
            return
        }
        await LoadingTimer("Notifikasi berhasil dikirim", "success", 1500);

    };

    return (
        <TitlePages head={"Notifikasi"} nama={nama}>
            <section className="section dashboard">
                <div className="row">
                    <div className="col-lg-12">
                        <form>
                            <div className="form-group mb-3">
                                <label htmlFor="judul">Judul</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="judul"
                                    name="judul"
                                    onChange={(e) => setJudul(e.target.value)}
                                    autoFocus
                                />
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="isi">Isi</label>
                                <textarea
                                    className="form-control"
                                    id="isi"
                                    name="isi"
                                    onChange={(e) => setIsi(e.target.value)}
                                />
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="gambar">URL Gambar</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="gambar"
                                    name="gambar"
                                    onChange={(e) => setGambar(e.target.value)}
                                />
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="tujuan">URL Tujuan</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="tujuan"
                                    name="tujuan"
                                    onChange={(e) => setTujuan(e.target.value)}
                                />
                            </div>
                            <center>
                                <button
                                    type="button"
                                    className="btn btn-primary"
                                    onClick={handleNotif}
                                >
                                    Buat Notifikasi
                                </button>
                            </center>
                        </form>
                    </div>
                </div>
            </section>
        </TitlePages>
    );
};

export default Notifikasi;