import type {NextApiRequest, NextApiResponse} from 'next';
import formidable from 'formidable';
import CekLogin from "@/utils/cek_login";
import {Telegraf} from 'telegraf'
import moment from "moment";
import {KirimPesan} from "mazaha";

const bot = new Telegraf(process.env.NEXT_PUBLIC_TELEGRAM_BOT_TOKEN ?? '');
const tujuan = process.env.NEXT_PUBLIC_TELEGRAM_TO_ID ?? '';

export const config = {
    api: {
        bodyParser: false,
    }
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    const server = req.headers.host ?? '';
    const ceklogin = await CekLogin(req.cookies.token);
    if (!ceklogin.success) {
        res.status(401).json({error: 'Unauthorized'});
        return;
    }
    if (req.method === 'POST') {
        const form = formidable();
        form.parse(req, async (err: any, fields: any, files: any) => {
            if (err) {
                console.error('Error parsing form:', err);
                res.status(500).json({error: 'Internal server error'});
            } else {
                if (!fields.pesan) {
                    res.status(400).json({error: 'Pesan harus diisi'});
                    return;
                }
                const date = moment().format('DD-MM-YYYY : HH:mm:ss');
                const kirim = await KirimPesan({
                    bot_token: process.env.NEXT_PUBLIC_TELEGRAM_BOT_TOKEN ?? '',
                    id: process.env.NEXT_PUBLIC_TELEGRAM_TO_ID ?? '',
                    pesan: fields.pesan,
                    pengirim: server,
                    waktu: date,
                })
                if (kirim.success) {
                    res.status(200).json({success: true, data: kirim.data});
                } else {
                    res.status(500).json({success: false, error: 'Internal server error, koneksi ke telegram gagal'});
                }
            }
        });
    } else {
        res.status(400).json({methode: req.method})
    }
}