import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import React from "react";

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <main id="main" className="main">
        <div className="pagetitle">
          <h1>Dashboard</h1>
          <nav>
            <ol className="breadcrumb">
              {/*<li className="breadcrumb-item"><a href="{{ $url_panel.'/dashboard' }}">Home</a></li>*/}
              {/*<li className="breadcrumb-item active">XXX</li>*/}
            </ol>
          </nav>
        </div>
      </main>
    </>
  )
}
