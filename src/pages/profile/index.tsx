import React from 'react';
import TitlePages from "@/components/title";
import useSWR from "swr";
import axios from "axios";
import {getCookie} from "cookies-next";
import Spinner from "@/components/spinner";
import Profile from "@/components/card/profile";

const Index = () => {
    const nama = "Profile";
    const {data, error, isLoading} = useSWR(`${process.env.NEXT_PUBLIC_API_URL}/auth/detail`, async (url) => {
        const res = await axios.post(url, {}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token') ?? ''
            }
        });
        return await res.data.data;
    });
    return (
        <TitlePages head={"Profile"} nama={nama}>
            <section className="section dashboard">
                <div className="row">
                    <center>
                        {isLoading && (
                            <div id="loading" className="loading-container">
                                <Spinner text={"Megambil Data."}/>
                            </div>
                        )}
                        {error && (
                            <div id="loading" className="loading-container">
                                <Spinner text={"Terjadi Kesalahan."}/>
                            </div>
                        )}
                        {data && (
                            <Profile data={data} />
                        )}
                    </center>
                </div>
            </section>
        </TitlePages>
    );
};

export default Index;