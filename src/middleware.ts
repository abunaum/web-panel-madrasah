import {NextResponse} from 'next/server';
import type {NextRequest} from 'next/server';

// This function can be marked `async` if using `await` inside
export async function middleware(request: NextRequest) {
    const token = request.cookies.get('token')?.value;
    const {pathname} = request.nextUrl;
    switch (pathname) {
        case '/auth':
            if (token) {
                const cek = await cekToken(token);
                if (cek.server) {
                    if (cek.success) {
                        return NextResponse.redirect(new URL('/', request.url))
                    }
                    request.cookies.delete('token')
                    return NextResponse.next()
                } else {
                    return NextResponse.redirect(new URL('/gangguan', request.url))
                }
            } else {
                return NextResponse.next()
            }
        default:
            if (!token) {
                return NextResponse.redirect(new URL('/auth', request.url))
            }
            const cek = await cekToken(token);
            if (cek.server) {
                if (cek.success) {
                    return NextResponse.next()
                }
                request.cookies.delete('token')
                return NextResponse.redirect(new URL('/auth', request.url))
            } else {
                return NextResponse.redirect(new URL('/gangguan', request.url))
            }
    }
}

const cekToken = async (token: string | boolean) => {
    try {
        // fetch data from a url endpoint don't use axios
        const cek = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/auth/detail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        });
        const test = await cek.json();
        if (!test.success) {
            return {
                success: false,
                server: true,
            }
        }
        return {
            success: true,
            server: true,
        }
    } catch (error) {
        return {
            success: false,
            server: false,
        }
    }
}

// See "Matching Paths" below to learn more
export const config = {
    matcher: [
        /*
         * Match all request paths except for the ones starting with:
         * - api (API routes)
         * - _next/static (static files)
         * - _next/image (image optimization files)
         * - favicon.ico (favicon file)
         */
        '/((?!api|_next/static|_next/image|favicon.ico|assets|vercel|next).*)',
    ],
}