import {getCookie} from "cookies-next";
import axios from "axios";

type dataProps = {
    [key: string]: string|null|undefined;
};

export async function EditPost(data: dataProps, id: string | string[] | undefined): Promise<any> {
    Object.keys(data).forEach((key) => (data as any)[key] == null && delete (data as any)[key]);
    const token = getCookie('token');
    const api = process.env.NEXT_PUBLIC_API_URL ?? '';
    try {
        const edit = await axios.put(`${api}/post/${id}`, data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        });
        return edit.data;
    } catch (error) {
        console.error(error);
        return {
            success: false,
            error: error
        };
    }
}
