import axios from "axios";

const API_KEY = process.env.NEXT_PUBLIC_ONESIGNAL_API_KEY ?? "";
const ONESIGNAL_APP_ID = process.env.NEXT_PUBLIC_ONESIGNAL_APP_ID ?? "";
const BASE_URL = "https://onesignal.com/api/v1";
const optionsBuilder = (body: any) => {
    const new_body = {
        app_id: ONESIGNAL_APP_ID,
        included_segments: ['Active Subscriptions'],
        data: {},
        headings: {
            en: body.judul
        },
        contents: {
            en: body.isi,
        },
        url: body.tujuan,
        big_picture: body.gambar,
        chrome_web_image: body.gambar
    }
    let data;
    if (!body){
        data = null
    } else {
        data = JSON.stringify(new_body);
    }
    return data;
}

type dataProps = {
    isi: string,
    judul: string,
    gambar: string,
    tujuan: string
}
const createNotication = async (data: dataProps) => {
    const url = "https://onesignal.com/api/v1/notifications";
    const options = optionsBuilder(data);
    try {
        const response = await axios.post(url, options, {
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': `Basic ${API_KEY}`,
            }
        });
        return {
            success: true,
            data: response.data
        };
    } catch (error) {
        console.error(error);
        return {
            success: false,
            error: error
        };
    }
}


export default createNotication;