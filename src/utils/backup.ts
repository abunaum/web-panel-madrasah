import moment from 'moment';

export function backupData(filename: string, file: string) {
    const blob: Blob = new Blob([file], {type: 'text/plain'});
    const blobUrl: string = URL.createObjectURL(blob);
    const downloadLink: HTMLAnchorElement = document.createElement('a');
    downloadLink.href = blobUrl;
    const date: string = moment().format('YYYY-MM-DD-HH-mm-ss');
    downloadLink.download = `${filename}-backup-${date}.mazaha`;
    downloadLink.style.display = 'none';

    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}