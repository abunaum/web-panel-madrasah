import axios from 'axios';
import {getCookie} from "cookies-next";

type datatype = "gs" | "post";
export async function uploadBackup(type: datatype, data: any, setPercentage: (value: (((prevState: (number | boolean)) => (number | boolean)) | number | boolean)) => void) {
    if (type === "gs" || type === "post") {
        return processUploadBackup(type, data, setPercentage);
    } else {
        return { success: false, message: "Type is not valid" };
    }
}

async function processUploadBackup(type: string, data: any, setPercentage: (value: (((prevState: (number | boolean)) => (number | boolean)) | number | boolean)) => void) {
    const api_url = process.env.NEXT_PUBLIC_API_URL ?? "";
    if (!data) {
        return { success: false, message: "Data is empty" };
    }
    if (Object.keys(data).length !== 1) {
        return { success: false, message: "Data is not valid" };
    }

    const key = Object.keys(data)[0];
    const item = data[key];

    setPercentage(0);
    const batchSize = 10;
    const totalBatches = Math.ceil(item.length / batchSize);
    let completedBatches = 0;

    const processBatch = async (batch: any[], endpoint: string) => {
        const requests = batch.map(data => {
            return axios.post(`${api_url}/${endpoint}`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${getCookie('token')}`
                }
            });
        });

        try {
            await Promise.all(requests);
            completedBatches++;
            const progressPercentage = Math.round((completedBatches / totalBatches) * 100);
            setPercentage(progressPercentage);

            if (completedBatches === totalBatches) {
                setTimeout(() => {
                    setPercentage(false);
                }, 5000);
                return { success: true };
            }
        } catch (error) {
            return { success: false, message: error };
        }
    };

    const processAllBatches = async (data: any[], endpoint: string) => {
        for (let i = 0; i < totalBatches; i++) {
            const startIndex = i * batchSize;
            const endIndex = startIndex + batchSize;
            const batch = data.slice(startIndex, endIndex);
            await processBatch(batch, endpoint);
        }
    };

    try {
        await processAllBatches(item, type === "gs" ? "gs/tambah" : "post/tambah");
        return { success: true };
    } catch (error) {
        return { success: false, message: error };
    }
}

export async function Reset(type: string) {
    const api_url = process.env.NEXT_PUBLIC_API_URL ?? "";
    switch (type) {
        case "gs":
            try {
                const token = getCookie('token');
                const response = await axios.post(`${api_url}/gs/reset`, {token}, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    }
                });
                return {success: true, message: response.data};
            } catch (e) {
                return {success: false, message: e};
            }
        case "post":
            try {
                const token = getCookie('token');
                const response = await axios.post(`${api_url}/post/reset`, {token}, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    }
                });
                return {success: true, message: response.data};
            } catch (e) {
                return {success: false, message: e};
            }
        default:
            return {success: false, message: "Type is not valid"};
    }
}