import {create} from 'zustand';

interface AppState {
    judul: string;
    isi: string;
    gambar: string;
    tujuan: string;
    setJudul: (judul: string) => void;
    setIsi: (isi: string) => void;
    setGambar: (gambar: string) => void;
    setTujuan: (tujuan: string) => void;
}

export const notifState = create<AppState>((set) => ({
    judul: '',
    isi: '',
    gambar: '',
    tujuan: '',
    setJudul: (judul) => set({ judul }),
    setIsi: (isi) => set({ isi }),
    setGambar: (gambar) => set({ gambar }),
    setTujuan: (tujuan) => set({ tujuan }),
}));