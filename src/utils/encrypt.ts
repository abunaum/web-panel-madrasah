import {AES} from "crypto-js";

export function encryptData(filename: string, file: any){
    const secretKey: string = process.env.NEXT_PUBLIC_SECRET_KEY ?? "";
    const originalData = {[filename]: file};
    return AES.encrypt(JSON.stringify(originalData), secretKey).toString();
}
