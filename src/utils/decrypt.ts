import {AES, enc} from "crypto-js";

export async function decryptData(encryptedData: string){
    const secretKey: string = process.env.NEXT_PUBLIC_SECRET_KEY ?? "";
    try {
        const bytes = await JSON.parse(AES.decrypt(encryptedData, secretKey).toString(enc.Utf8));
        return {
            success: true,
            data: bytes
    }
    } catch (e) {
        return {
            success: false,
            message: e
        };
    }
}