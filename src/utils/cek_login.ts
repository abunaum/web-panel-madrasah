import React from 'react';

type CekLoginResponse = {
    success: boolean;
    server: boolean;
};

const CekLogin = async (token: string | undefined): Promise<CekLoginResponse> => {
    if (!token) {
        return {
            success: false,
            server: true,
        };
    }

    try {
        const cek = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/auth/detail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
        });
        const test = await cek.json();
        console.log(test)

        if (!test.success) {
            return {
                success: false,
                server: true,
            };
        }

        return {
            success: true,
            server: true,
        };
    } catch (error) {
        return {
            success: false,
            server: false,
        };
    }
};

export default CekLogin;
